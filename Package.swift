// swift-tools-version: 5.7.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "ABOLCodeScanner",
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .library(
            name: "ABOLCodeScanner",
            targets: [
                "ABOLCodeScanner"
            ]
        ),
    ],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
        .target(
            name: "ABOLCodeScanner",
            resources: [
                .process("Resources")
            ]
        ),
        .testTarget(
            name: "ABOLCodeScannerTests",
            dependencies: ["ABOLCodeScanner"]),
    ]
)
