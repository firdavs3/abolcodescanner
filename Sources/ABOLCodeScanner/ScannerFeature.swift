//
//  ScannerFeature.swift
//
//
//  Created by firdavs on 14.11.2023.
//
import AVFoundation
import UIKit

public final class ScannerFeature: NSObject {
    
    // MARK: - DI
    private var scannerCaptureVideoPreviewLayer: AVCaptureVideoPreviewLayer?
    private let captureSession = AVCaptureSession()
    private let scannerABOVCBuilder = ScannerVCBuilder.build()
    private var metadataObjectTypes: [AVMetadataObject.ObjectType] = []
    private let captureMetadataOutput = AVCaptureMetadataOutput()
    private let captureDevice = AVCaptureDevice.default(for: .video)
    private let parceCodeService = ParceCodeService()
    private let handleErrorService = HandleErrorService()
    
    // MARK: - Delegate
    public var scannerDelegate: ScannerDelegate?
    
    // MARK: - public methods
    
    public func run(with title: NSAttributedString) -> ScannerVC {
        let viewProperties = ScannerVC.ViewProperties(
            title: title,
            flashButtonImage: .set(name: .flashOff),
            viewWillAppear: viewWillAppear,
            viewWillDisappear: viewWillDisappear,
            didTapFlash: didTapTorchMode,
            addСaptureVideoPreviewLayer: addСaptureVideoPreviewLayer
        )
        
        scannerABOVCBuilder.viewManager.state = .createViewProperties(viewProperties)
        return scannerABOVCBuilder.view
    }

    public func setCodeTypes(
        types scanType: ScanType
    ){
        self.metadataObjectTypes = scanType.metadataObjectTypes()
    }
    
    public func stopScanBackground() {
        DispatchQueue.global(qos: .background).async {
            self.captureSession.stopRunning()
        }
    }
    
    public func startScanBackground() {
        DispatchQueue.global(qos: .background).async {
            self.captureSession.startRunning()
        }
    }
    
    private func addСaptureVideoPreviewLayer(with view: UIView) {
        guard scannerCaptureVideoPreviewLayer == nil else { return }
        
        setupCaptureDevice()
        setupCanaptureMetadataOutput()
        
        scannerCaptureVideoPreviewLayer = AVCaptureVideoPreviewLayer(
            session: captureSession
        )
        scannerCaptureVideoPreviewLayer!.frame = .init(
            x: 0,
            y: 0,
            width: view.layer.bounds.width + 100,
            height: view.layer.bounds.height
        )
        scannerCaptureVideoPreviewLayer!.videoGravity = .resizeAspect
        view.layer.insertSublayer(scannerCaptureVideoPreviewLayer!, at: 0)
    }
    
    private func setupCaptureDevice() {
        guard let captureDevice = self.captureDevice else { return }
        
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: captureDevice)
            if captureSession.canAddInput(videoInput) {
                captureSession.addInput(videoInput)
            }
        } catch {
            scannerDelegate?.errorScanner(with: .initScanner)
        }
    }
    
    private func setupCanaptureMetadataOutput() {
        if captureSession.canAddOutput(captureMetadataOutput) {
            captureSession.addOutput(captureMetadataOutput)
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = self.metadataObjectTypes
        }
    }
    
    private func didTapTorchMode(){
        guard captureDevice?.hasTorch == true else { return }
        do {
            try captureDevice?.lockForConfiguration()
            if captureDevice?.torchMode == .off {
                captureDevice?.torchMode = .on
                scannerABOVCBuilder.viewManager.state = .updateFlash(.set(name: .flashOn))
            } else {
                captureDevice?.torchMode = .off
                scannerABOVCBuilder.viewManager.state = .updateFlash(.set(name: .flashOff))
            }
            captureDevice?.unlockForConfiguration()
        } catch let error {
            print(error)
        }
    }
 
    private func viewWillAppear() {
        if captureSession.isRunning == false {
            startScanBackground()
        }
    }
    
    private func viewWillDisappear() {
        if captureSession.isRunning == true {
            stopScanBackground()
        }
    }
}

extension ScannerFeature: AVCaptureMetadataOutputObjectsDelegate {
    
    public func metadataOutput(
        _ output: AVCaptureMetadataOutput,
        didOutput metadataObjects: [AVMetadataObject],
        from connection: AVCaptureConnection
    ) {
        stopScanBackground()
        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else {
                self.scannerDelegate?.errorScanner(with: .readableCodeObject)
                return
            }
            guard let stringValue = readableObject.stringValue, !stringValue.isEmpty else {
                self.scannerDelegate?.errorScanner(with: .metadataObjects)
                return
            }
            
            if let customError = CustomError(rawValue: stringValue) {
                let scannerError = customError.scannerError()
                self.scannerDelegate?.errorScanner(with: scannerError)
                return
            }
           
            guard let code = parceCodeService.parse(
                with: stringValue)
            else {
                self.scannerDelegate?.errorScanner(with: .metadataObjects)
                return
            }
            
            if let customError = self.handleErrorService.endHandle(
                with: code,
                objectType: metadataObjectTypes
            ) {
                let scannerError = customError.scannerError()
                self.scannerDelegate?.errorScanner(with: scannerError)
                return
            }
            
            self.scannerDelegate?.successScanner(with: code)
        } else {
            self.scannerDelegate?.errorScanner(with: .metadataObjects)
        }
    }
}
