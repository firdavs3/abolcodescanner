//
//  ScannerVCManager.swift
//  
//
//  Created by firdavs on 14.11.2023.
//

import UIKit

final class ScannerVCManager: ViewManager<ScannerVC> {
    
    var viewProperties: ScannerVC.ViewProperties?
    
    // MARK: - Main state view model
    enum State {
        case createViewProperties(ScannerVC.ViewProperties)
        case updateFlash(UIImage?)
    }
    
    public var state: State? {
        didSet { self.stateManager() }
    }
    
    private func stateManager() {
        guard let state = self.state else { return }
        switch state {
            case .createViewProperties(let viewProperties):
            self.viewProperties = viewProperties
            create?(self.viewProperties)
            
        case .updateFlash(let flashButtonImage):
            viewProperties?.flashButtonImage = flashButtonImage
            update?(self.viewProperties)
        }
    }
}

