//
//  ParceCode.swift
//
//
//  Created by firdavs on 20.11.2023.
//
import AVFoundation
import Foundation

public struct ParceCodeService {
    
    private let filterService = FilterService()
    
    public init() {}
    
    public func parse(with stringValue: String) -> String? {
        let result = filterService.filter(with: stringValue)
        return result
    }
}
