//
//  ScannerDelegate.swift
//
//
//  Created by firdavs on 14.11.2023.
//

import Foundation

public protocol ScannerDelegate {
    
    func successScanner(with code: String)
    func errorScanner(with scannerError: ScannerError)
}
