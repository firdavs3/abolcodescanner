//
//  ScannerVC.swift
//  
//
//  Created by firdavs on 14.11.2023.
//

import UIKit
import AVFoundation

public final class ScannerVC: UIViewController, ViewProtocol {
    
    // MARK: - Main ViewProperties
    struct ViewProperties {
        let title: NSAttributedString
        var flashButtonImage: UIImage?
        let viewWillAppear: ClosureEmpty
        let viewWillDisappear: ClosureEmpty
        let didTapFlash: ClosureEmpty
        let addСaptureVideoPreviewLayer: Closure<UIView>
    }
    var viewProperties: ViewProperties?
    
    public override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    public override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: Init
    
    init() {
        super.init(nibName: String(describing: Self.self), bundle: Bundle.module)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    // MARK: UI
    @IBOutlet weak private var titleLabel: UILabel!
    @IBOutlet weak private var flashButton: UIButton!
    @IBOutlet weak private var rectangleScanerView: UIView!
    
    // MARK: - LifeCycle
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewProperties?.viewWillAppear()
    }

    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewProperties?.viewWillDisappear()
    }
    
    // MARK: - public methods
    
    func update(with viewProperties: ViewProperties?) {
        self.viewProperties = viewProperties
        setTitle(with: viewProperties)
        setFlashButton(with: viewProperties)
    }
    
    func create(with viewProperties: ViewProperties?) {
        self.viewProperties = viewProperties
        viewProperties?.addСaptureVideoPreviewLayer(self.view)
        setTitle(with: viewProperties)
        setFlashButton(with: viewProperties)
    }
    
    // MARK: - private methods
    
    private func setTitle(with viewProperties: ViewProperties?){
        self.titleLabel.attributedText = viewProperties?.title
    }
    
    private func setFlashButton(with viewProperties: ViewProperties?){
        self.flashButton.setBackgroundImage(viewProperties?.flashButtonImage, for: .normal)
    }
    
    //MARK: - Buttons
    
    @IBAction func flashButton(button: UIButton){
        self.viewProperties?.didTapFlash()
    }
}

