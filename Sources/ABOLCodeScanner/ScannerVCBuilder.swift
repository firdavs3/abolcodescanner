//
//  ScannerVCBuilder.swift
//  
//
//  Created by firdavs on 14.11.2023.
//

import UIKit

final class ScannerVCBuilder: BuilderProtocol {
    
    typealias V  = ScannerVC
    typealias VM = ScannerVCManager
    
    public var view: ScannerVC
    public var viewManager: ScannerVCManager
    
    public static func build() -> ScannerVCBuilder {
        let viewController = ScannerVC()
        let viewManager = ScannerVCManager()
        viewController.loadViewIfNeeded()
        viewManager.bind(with: viewController)
        let selfBuilder = ScannerVCBuilder(
            with: viewController,
            with: viewManager
        )
        return selfBuilder
    }
    
    private init(
        with viewController: ScannerVC,
        with viewManager: ScannerVCManager
    ) {
        self.view        = viewController
        self.viewManager = viewManager
    }
}
