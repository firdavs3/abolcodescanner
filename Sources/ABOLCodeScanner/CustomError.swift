//
//  CustomError.swift
//  
//
//  Created by firdavs on 17.11.2023.
//

import Foundation

public enum CustomError: String {
    
    case customQrError
    case customBurcodeError
    case КБКError
    
    func scannerError() -> ScannerError {
        switch self {
            
        case .customBurcodeError:
            return .customBurcodeError
            
        case .customQrError:
            return .customQrError
            
        case .КБКError:
            return .КБКError
        }
    }
}
