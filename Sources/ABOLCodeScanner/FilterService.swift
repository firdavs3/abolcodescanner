//
//  FilterService.swift
//
//
//  Created by firdavs on 01.12.2023.
//

import Foundation

public struct FilterService {
    
    public init() { }
    
    public func filter(with stringValue: String) -> String? {
        var stringCodes: Set<String> = []
        
        // 1 - сканируем простой код
        if let simpleCode = simple(with: stringValue){
            return simpleCode
        }
        
        // 2 - сканируем ацтек
        if let filterDocIDx = FilterKey.docIDx.filter(with: stringValue) {
            stringCodes.insert(filterDocIDx)
        }
        
        // 3 - сканируем PDF
        let filterCode = FilterCode.filter(with: stringValue)
        
        filterCode.forEach { stringCodes.insert($0) }
        
        return stringCodes.first
    }
    
    private func simple(with stringValue: String) -> String? {
        let codes = stringValue.split(separator: "|").compactMap { String($0) }
        if codes.count == 1 {
            return stringValue
        } else {
            return nil
        }
    }
    
    public enum FilterCode: String, CaseIterable {
        
        case КБКCode = "18201061201010000510"
        case КБКPrefix = "182"
        
        static func filter(with stringValue: String) -> Set<String> {
            let codes = stringValue.split(separator: "|").compactMap { String($0) }
            var setCodes: Set<String> = []
            
            let filterHasPrefixCoes = FilterCode.КБКPrefix.hasPrefix(with: codes)
            let filterDeleteCoes = FilterCode.КБКCode.delete(with: filterHasPrefixCoes)
           
            filterDeleteCoes.forEach{ setCodes.insert($0) }
            
            return setCodes
        }
        
        private func hasPrefix(with codes: [String]) -> [String] {
            let filterCodes = codes.filter { $0.hasPrefix(self.rawValue) }
            let stringCodes = filterCodes.map({ String($0 )})
            return stringCodes
        }
        
        private func delete(with codes: [String]) -> [String] {
            let filterCodes = codes.filter { $0 != self.rawValue }
            let stringCodes = filterCodes.map({ String($0 )})
            return stringCodes
        }
    }
    
    private enum FilterKey: String, CaseIterable {
        
        case docIDx = "docIdx"
        
        func filter(with stringValue: String) -> String? {
            let codes = stringValue.split(separator: "|")
            if let filterDocIDx = codes.first(where: {
                String($0).contains(self.rawValue)
            })?.split(separator: "=").last, !filterDocIDx.isEmpty {
                return String(filterDocIDx)
            }
            return nil
        }
    }

}

