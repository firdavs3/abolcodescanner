//
//  Extensions.swift
//
//
//  Created by firdavs on 16.11.2023.
//
import UIKit

extension UIImage {
    
    static func set(name: Name) -> UIImage? {
        let image = UIImage(
            named: name.rawValue,
            in: Bundle.module,
            compatibleWith: nil
        )
        return image
    }
    
    enum Name: String {
        case flashOff
        case flashOn
    }
}
