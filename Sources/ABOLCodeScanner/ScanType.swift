//
//  ScanType.swift
//  
//
//  Created by firdavs on 16.11.2023.
//
import AVFoundation
import Foundation

public enum ScanType {
    case qr
    case barcode
    case all
    case face
    
    public func metadataObjectTypes() -> [AVMetadataObject.ObjectType] {
        switch self {
        case .face:
            return [
                .face
            ]
            
        case .qr:
            return [
                .aztec,
                .qr
            ]
        case .barcode:
            return [
                .code128,
                .code39,
                .code39Mod43,
                .code93,
                .pdf417
            ]
        case .all:
            
            var arrayCodes: [AVMetadataObject.ObjectType] = [
                .code39,
                .code39Mod43,
                .code93,
                .code128,
                .ean8,
                .ean13,
                .interleaved2of5,
                .itf14,
                .upce,
                .aztec,
                .dataMatrix,
                .pdf417,
                .qr
            ]
            
            if #available(iOS 15.4, *) {
                let codes: [AVMetadataObject.ObjectType] = [
                    .codabar,
                    .gs1DataBar,
                    .gs1DataBarExpanded,
                    .gs1DataBarLimited,
                    .microPDF417,
                    .microQR
                ]
                arrayCodes.append(contentsOf: codes)
            }
//            
//            if #available(iOS 13, *) {
//                let codes: [AVMetadataObject.ObjectType] = [
//                   // .salientObject,
//                    .dogBody,
//                    .catBody,
//                    .humanBody
//                ]
//                arrayCodes.append(contentsOf: codes)
//            }
            
            return arrayCodes
        }
    }
}
//Barcodes
//static let codabar
//A constant that identifies the Codabar symbology.
//static let code39
//A constant that identifies the Code 39 symbology.
//static let code39Mod43
//A constant that identifies the Code 39 mod 43 symbology.
//static let code93
//A constant that identifies the Code 93 symbology.
//static let code128
//A constant that identifies the Code 128 symbology.
//static let ean8
//A constant that identifies the EAN-8 symbology.
//static let ean13
//A constant that identifies the EAN-13 symbology.
//static let gs1DataBar
//A constant that identifies the GS1 DataBar symbology.
//static let gs1DataBarExpanded
//A constant that identifies the GS1 DataBar Expanded symbology.
//static let gs1DataBarLimited
//A constant that identifies the GS1 DataBar Limited symbology.
//static let interleaved2of5
//A constant that identifies the Interleaved 2 of 5 symbology.
//static let itf14
//A constant that identifies the ITF14 symbology.
//static let upce
//A constant that identifies the UPC-E symbology.
//2D Codes
//static let aztec
//A constant that identifies the Aztec symbology.
//static let dataMatrix
//A constant that identifies the DataMatrix symbology.
//static let microPDF417
//A constant that identifies the Micro PDF417 symbology.
//static let microQR
//A constant that identifies the Micro QR symbology.
//static let pdf417
//A constant that identifies the PDF417 symbology.
//static let qr
//A constant that identifies the QR symbology.
//Bodies
//static let humanBody
//A constant that identifies human body metadata.
//static let humanFullBody
//A constant that identifies human full body metadata.
//static let dogBody
//A constant that identifies dog body metadata.
//static let catBody
//A constant that identifies cat body metadata.
//Faces
//static let face
//A constant that identifies face metadata.
//Saliency
//static let salientObject
//A constant that identifies saliency metadata.
