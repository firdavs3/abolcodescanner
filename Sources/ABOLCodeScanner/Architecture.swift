//
//  Architecture.swift
//  
//
//  Created by firdavs on 14.11.2023.
//

import Foundation

internal protocol BuilderProtocol: AnyObject {
    
    associatedtype V : ViewProtocol
    associatedtype VM: ViewManager<V>
    
    var viewManager: VM { get set }
    var view       : V  { get set }
}

//MARK: - CLOUSURES
internal typealias ClosureReturn<T> = (() -> T)
internal typealias Closure<T>       = ((T) -> Void)
internal typealias ClosureEmpty     = (() -> Void)
internal typealias ClosureTwo<T, G> = ((T, G) -> Void)
internal typealias ClosureAny       = ((Any?) -> Void)

internal class ViewManager<View: ViewProtocol> {

    public var update: Closure<View.ViewProperties?>?
    public var create: Closure<View.ViewProperties?>?

    // MARK: - Привязываем View с ViewModel
    public func bind(with view: View) {
        self.update = view.update(with:)
        self.create = view.create(with:)
    }

    public init(){}
}

internal protocol ViewProtocol where Self: AnyObject {
    
    associatedtype ViewProperties
    
    func create(with viewProperties: ViewProperties?)
    func update(with viewProperties: ViewProperties?)
}
