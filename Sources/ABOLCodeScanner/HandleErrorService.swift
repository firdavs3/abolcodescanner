//
//  HandleErrorService.swift
//
//
//  Created by firdavs on 01.12.2023.
//
import AVFoundation

public struct HandleErrorService {
    
    public init() {}
    
    public func endHandle(with codeString: String, objectType: [AVMetadataObject.ObjectType]) -> CustomError? {
        
        switch true {
        case objectType.contains(where: { $0 == .aztec }) :
            if !codeString.hasPrefix(FilterService.FilterCode.КБКPrefix.rawValue) {
                return .КБКError
            }
            
            if codeString.contains(FilterService.FilterCode.КБКCode.rawValue) {
                return .КБКError
            }
            
            if codeString.hasPrefix(FilterService.FilterCode.КБКPrefix.rawValue)
                && codeString.contains(Code.КБКCode.rawValue) {
                return .КБКError
            }
            
        default:
            return nil
        }
        
        return nil
    }
    
    public enum HandleError {
        case aztec
    }
    
    private enum Code: String, CaseIterable {
        case КБКCode = "18201061201010000510"
        case КБКPrefix = "182"
    }
}
