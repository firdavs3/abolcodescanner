//
//  ScannerError.swift
//  
//
//  Created by firdavs on 16.11.2023.
//

import Foundation

public enum ScannerError: Error {
    
    case metadataObjects
    case readableCodeObject
    case readableObjectString
    case initScanner
    case customQrError
    case customBurcodeError
    case КБКError
}
